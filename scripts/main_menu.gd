extends Control


onready var world = preload("res://scenes/World.tscn").instance()


func _on_Play_button_up():
	get_tree().get_root().add_child(world)
	hide()


func _on_Exit_button_up():
	get_tree().quit()
