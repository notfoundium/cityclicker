extends Spatial

onready var observer = $Observer
onready var grid = $Grid
onready var gui = $Observer/CanvasLayer/GUI
onready var sounds = $Sounds

export var map_size: int = 8

var max_money: int = 0
var money: int = 0
var max_pop: int = 0
var pops: int = 0
var max_materials: int = 10
var materials: int = 10
var max_food: int = 0
var food: int = 0
var max_research: int = 0
var research: int = 0

var buildings_count: int = 0

const tile_size = 4
const offset = tile_size * 2
const camera_h = 20


func _ready():
	grid.init(map_size, tile_size)
	gui.update_materials(materials)
	gui.update_max_materials(max_materials)
	observer.translation = Vector3(map_size / 2 * tile_size + offset,
									camera_h,
									map_size / 2 * tile_size - offset)


func sound_click():
	sounds.play()


func set_max_money(value: int):
	max_money = value
	
	if money > max_money:
		money = max_money
	gui.update_max_money(max_money)
	gui.update_money(money)


func add_money(count: int):
	if money + count > max_money:
		money = max_money
	else:
		money += count
	gui.update_money(money)


func set_max_pop(value: int):
	max_pop = value
	
	if pops > max_pop:
		pops = max_pop
	gui.update_max_pop(max_pop)
	gui.update_pops(pops)


func add_pops(count: int):
	if food - count < 0:
		gui.warn("Not enough food")
	elif pops + count > max_pop:
		pass
	else:
		food -= count
		pops += count
	gui.update_pops(pops)
	gui.update_food(food)


func set_max_materials(value: int):
	max_materials = value
	
	if materials > max_materials:
		materials = max_materials
	gui.update_max_materials(max_materials)
	gui.update_materials(materials)


func add_materials(count: int):
	if money - count * 10 < 0:
		gui.warn("Not enough money")
	elif materials + count > max_materials:
		pass
	else:
		money -= count * 10
		materials += count
	gui.update_materials(materials)
	gui.update_money(money)


func reduce_materials(count: int) -> bool:
	if materials - count < 0:
		gui.warn("Not enough materials!")
		return false
	materials -= count
	gui.update_materials(materials)
	return true


func set_max_food(value: int):
	max_food = value
	
	if food > max_food:
		food = max_food
	gui.update_max_food(max_food)
	gui.update_food(food)


func add_food(count: int):
	if food + count > max_food:
		food = max_food
		pass
	else:
		food += count
	gui.update_food(food)


func set_max_research(value: int):
	max_research = value
	gui.update_max_research(max_research)


func add_research(count: int):
	if research + count > max_research:
		research = max_research
		pass
	else:
		research += count
	gui.update_research(research)
