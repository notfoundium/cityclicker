extends Spatial


enum e_buildings {
	EMPTY,
	HOUSE,
	MARKET,
	FACTORY,
	FARM,
	BARN,
	WAREHOUSE,
	LAB,
	LIBRARY,
	OFFICE
}

var is_under_construction: bool
var construction_progress: int
var building = e_buildings.EMPTY
var count: int = 0
var index: int


onready var building_model = $building_model
onready var construction_model = preload("res://res/models/construction.tres")
onready var house_model = preload("res://res/models/house.tres")
onready var market_model = preload("res://res/models/market.tres")
onready var factory_model = preload("res://res/models/factory.tres")
onready var farm_model = preload("res://res/models/farm.tres")
#onready var lab_model = preload("res://res/models/lab.tres")
onready var barn_model = preload("res://res/models/barn.tres")
onready var warehouse_model = preload("res://res/models/warehouse.tres")
#onready var library_model = preload("res://res/models/library.tres")

onready var world = get_tree().get_root().get_node("/root/World")
onready var grid = get_parent()


var tmp = 0


func _unhandled_key_input(event):
	if event.is_action_pressed("demolish"):
		tmp = 0
	if event.is_action_pressed("house"):
		tmp = e_buildings.HOUSE
	if event.is_action_pressed("market"):
		tmp = e_buildings.MARKET
	if event.is_action_pressed("factory"):
		tmp = e_buildings.FACTORY
	if event.is_action_pressed("farm"):
		tmp = e_buildings.FARM
	if event.is_action_pressed("barn"):
		tmp = e_buildings.BARN
	if event.is_action_pressed("warehouse"):
		tmp = e_buildings.WAREHOUSE


func _on_area_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton or event is InputEventAction:
		if event.button_index == BUTTON_LEFT and event.pressed == true:
			click()
		if event.button_index == BUTTON_RIGHT and event.pressed == true:
			if tmp == 0:
				demolish()
			elif not is_under_construction:
				build(tmp)


func click():
	if is_under_construction and construction_progress >= 10:
		complete_construction()
		world.sound_click()
		return
		
	if is_under_construction:
		construction_progress += 1
		world.sound_click()
		return
		
	match building:
		e_buildings.HOUSE:
			world.add_pops(1)
			world.sound_click()
		e_buildings.MARKET:	
			world.add_money(1)
			world.sound_click()
		e_buildings.FACTORY:
			world.add_materials(1)
			world.sound_click()
		e_buildings.FARM:
			world.add_food(1)
			world.sound_click()
		e_buildings.LAB:
			world.add_research(1)
			world.sound_click()


func build(building: int):
	if building != e_buildings.HOUSE and building != e_buildings.FARM:
		if world.pops < world.buildings_count * 5:
			world.gui.warn("Not enough population!")
			return
	if not world.reduce_materials(1):
		return
	
	world.buildings_count += 1
	self.building = building
	building_model.mesh = construction_model
	is_under_construction = true
	construction_progress = 0


func complete_construction():
	is_under_construction = false
	match building:
		e_buildings.HOUSE:
			building_model.mesh = house_model
			world.set_max_pop(world.max_pop + 10)
		e_buildings.MARKET:
			building_model.mesh = market_model
			world.set_max_money(world.max_money + 20)
		e_buildings.FACTORY:
			building_model.mesh = factory_model
			world.set_max_materials(world.max_materials + 10)
		e_buildings.FARM:
			building_model.mesh = farm_model
			world.set_max_food(world.max_food + 10)
		e_buildings.BARN:
			building_model.mesh = barn_model
			world.set_max_food(world.max_food + 100)
		e_buildings.WAREHOUSE:
			building_model.mesh = warehouse_model
			world.set_max_materials(world.max_materials + 100)


func demolish():
	match building:
		e_buildings.HOUSE:
			world.set_max_pop(world.max_pop - 10)
		e_buildings.MARKET:
			world.set_max_money(world.max_money - 10)
		e_buildings.FACTORY:
			world.set_max_materials(world.max_materials - 10)
		e_buildings.FARM:
			world.set_max_food(world.max_food - 10)
		e_buildings.BARN:
			world.set_max_food(world.max_food - 100)
		e_buildings.WAREHOUSE:
			world.set_max_materials(world.max_materials - 100)
	building = e_buildings.EMPTY
	building_model.mesh = null


func _on_Timer_timeout():
	if building == e_buildings.BARN:
		grid.autoclick(index, e_buildings.FARM)
	if building == e_buildings.HOUSE:
		grid.autoclick(index, e_buildings.MARKET)
