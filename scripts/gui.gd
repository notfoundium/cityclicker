extends Control

onready var max_money = $stats/stats_rows/money/max
onready var money = $stats/stats_rows/money/value
onready var max_pop = $stats/stats_rows/pops/max
onready var pops = $stats/stats_rows/pops/value
onready var max_materials = $stats/stats_rows/materials/max
onready var materials = $stats/stats_rows/materials/value
onready var max_food = $stats/stats_rows/food/max
onready var food = $stats/stats_rows/food/value
onready var max_research = $stats/stats_rows/research/max
onready var research = $stats/stats_rows/research/value
onready var panel = $MarginContainer
onready var logger = $MarginContainer/log
onready var timer = $Timer


func warn(text: String):
	panel.visible = true
	logger.text = text
	panel.set_position(get_local_mouse_position())
	timer.start()


func update_max_money(value: int):
	max_money.text = str(value)


func update_money(value: int):
	money.text = str(value)


func update_max_pop(value: int):
	max_pop.text = str(value)


func update_pops(value: int):
	pops.text = str(value)


func update_max_materials(value: int):
	max_materials.text = str(value)


func update_materials(value: int):
	materials.text = str(value)


func update_max_food(value: int):
	max_food.text = str(value)


func update_food(value: int):
	food.text = str(value)


func update_max_research(value: int):
	max_research.text = str(value)


func update_research(value: int):
	research.text = str(value)


func _on_Timer_timeout():
	panel.visible = false
