extends Spatial


onready var tile = preload("res://scenes/Tile.tscn")

var tilemap = []

var map_size


func init(map_size: int, tile_size: int):
	self.map_size = map_size
	for i in range(map_size * map_size):
		tilemap.append(tile.instance())
		add_child(tilemap[i])
	
	var pos = 0
	for tile in tilemap:
		tile.translation = Vector3((pos % map_size) * tile_size + tile_size / 2,
									0,
									(pos / map_size) * tile_size + tile_size / 2)
		tile.index = pos
		pos += 1


func autoclick(index: int, target: int):
	for i in range(-1, 2):
		for j in range (-1, 2):
			var target_index = (index + i * map_size) + j
			if target_index < map_size * map_size and tilemap[target_index].building == target:
				tilemap[target_index].click()
